﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TinyIoC;

namespace QuicKE_Admin
{
      [ImplementPropertyChanged]
     public class StartedTasksViewModel
    {
         public static int TicketId { get; set; }
         public string Location { get; set; }
         public DateTime BookingDate { get; set; }
         public ObservableCollection<Service> Services { get; set; }
         public decimal TotalCost { get; set; }
         public ICommand CompleteCommand { get; private set; }
         ErrorBucket errors = new ErrorBucket();
         public StartedTasksViewModel()
         {
             //Services = new ObservableCollection<Service>();
             //Services.Add(new Service() { id = 1, name = "Laundry", cost = 40 });
             //Services.Add(new Service() { id = 2, name = "Java", cost = 50 });
             //Services.Add(new Service() { id = 3, name = "Market", cost = 500 });
             //Services.Add(new Service() { id = 4, name = "Random", cost = 400 });
             this.Services = new ObservableCollection<Service>();
             
         }

         public async Task LoadData()
         {

             var proxy = TinyIoCContainer.Current.Resolve<IStartTicketServiceProxy>();
             var result = await proxy.StartTaskAsync();
             if (!(result.HasErrors))
             {
                 this.Location = result.Ticket.location;
                 StartedTasksViewModel.TicketId = result.Ticket.id;
                 this.TotalCost = result.Ticket.cost;
                 this.BookingDate = DateTime.Parse(result.Ticket.booking_date);
                 foreach (var service in result.Ticket.services)
                 {
                     this.Services.Add(service);
                 }

                 

             }
             else
             {
                 errors.CopyFrom(result);
                 await PageExtender.ShowAlertAsync(errors);
             }
         }
    }
}
