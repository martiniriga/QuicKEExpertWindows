﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TinyIoC;
using Windows.Devices.Geolocation;


namespace QuicKE_Admin
{
    [ImplementPropertyChanged]
     public class HomePageViewModel
    {
         public static int TicketId { get; set; }
         public string Location { get; set; }
         public string HasTicket { get; set; }
         public string Status { get; set; }
         public DateTime BookingDate { get; set; }
         public ObservableCollection<Service> Services { get; set; }
         public decimal TotalCost { get; set; }

         ErrorBucket errors = new ErrorBucket();
         public HomePageViewModel()
         {
             this.Services = new ObservableCollection<Service>();
             TicketId = 1;
             this.TotalCost = 0;
             this.Location = "None";
             this.HasTicket = "No pending tasks";
         }

         public async Task LoadData()
         {

             var proxy = TinyIoCContainer.Current.Resolve<IGetTicketServiceProxy>();             
             var result = await proxy.GetTicketsAsync();
             if (result == null)
             {

             }
             if (!(result.HasErrors))
             {
                 if (result != null)
                 {                        
                     this.HasTicket = "A new request has been created for you click on Confirm below to accept task";
                     this.Status = result.Ticket.status;
                     this.Location = result.Ticket.location;
                     HomePageViewModel.TicketId = result.Ticket.id;
                     this.TotalCost = result.Ticket.cost;
                     this.BookingDate = DateTime.Parse(result.Ticket.booking_date);

                     foreach (Service service in result.Ticket.services)                     
                         this.Services.Add(service);
                     
                     
                 } 

             }
             else
             {
                 errors.CopyFrom(result);
                 await PageExtender.ShowAlertAsync(errors);
             }
         }
                 
    }
}
