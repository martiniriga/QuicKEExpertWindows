﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TinyIoC;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace QuicKE_Admin
{
    
    public sealed partial class PendingPage : Page
    {
        private StartedTasksViewModel vm = new StartedTasksViewModel();
        public PendingPage()
        {
            this.InitializeComponent();
            DataContext = null;
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
               HomePageViewModel hm = e.Parameter as HomePageViewModel;
               Loading.Visibility = Visibility.Visible;
               if (hm != null)
               {
                   //vm = new StartedTasksViewModel
                   //{
                   //    Services = hm.Services,
                   //    BookingDate = hm.BookingDate,
                   //    TotalCost = hm.TotalCost
                   //};
                   //StartedTasksViewModel.TicketId = HomePageViewModel.TicketId;
                   DataContext = hm;
               }
               else
               {
                   DataContext = vm;
                   await vm.LoadData();
               }          
                    

            Loading.Visibility = Visibility.Collapsed;

        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack)
                Frame.GoBack();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            ErrorBucket errors = new ErrorBucket();
            var proxy = TinyIoCContainer.Current.Resolve<ICompleteTicketServiceProxy>();
            var result = await proxy.GetTicketsAsync();
            errors.AssertNoErrors();            
        }
    }
}
