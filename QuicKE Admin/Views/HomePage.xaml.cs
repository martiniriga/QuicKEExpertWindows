﻿using QuicKE_Admin.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using TinyIoC;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace QuicKE_Admin
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HomePage : Page
    {
        private HomePageViewModel vm = new HomePageViewModel();
        public HomePage()
        {
            this.InitializeComponent();
            DataContext = vm;
        }


        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            //DataContext = null;
            Loading.Visibility = Visibility.Visible;
            await vm.LoadData();
            if (vm.Status == "Started")
            {
                Frame.Navigate(typeof(PendingPage), vm);
            }
                
            //if (HomePageViewModel.HasData)
            //{
            //    ServicePanel.Visibility = Visibility.Visible;
            //}
            //else
            //    Pending.Visibility = Visibility.Visible;

            Loading.Visibility = Visibility.Collapsed;
            //bg locationservice
            //var location = new Geolocator();
            //location.PositionChanged += location_PositionChanged;
            
        }
        //async void location_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        //{
        //    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
        //    {
        //        Geoposition position = args.Position;
                

        //    });
        //}
        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement senderElement = sender as FrameworkElement;
            MySettingsFlyOut.ShowAt(senderElement);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(PendingPage));
        }

        private async Task LogOut()
        {
            ErrorBucket errors = new ErrorBucket();
            var proxy = TinyIoCContainer.Current.Resolve<ILogOutServiceProxy>();
           Loading.Visibility = Visibility.Visible;
                var result = await proxy.LogOutAsync();
                if (!(result.HasErrors))
                {
                    var conn = QuicKERuntime.GetSystemDatabase();
                    var setting = (await conn.Table<SettingItem>().Where(v => v.Name == "LogonToken").ToListAsync()).FirstOrDefault();
                    if (setting != null)
                        await conn.DeleteAsync(setting);
                    QuicKERuntime.LogonToken = null;
                    errors.AddError("Logged Out Successfully");
                    errors.AssertNoErrors();
                    Frame.Navigate(typeof(LoginPage));
                }
                else
                    errors.CopyFrom(result);
                Loading.Visibility = Visibility.Collapsed;
            // errors?
                errors.AssertNoErrors();
        }
    }
}
