﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using TinyIoC;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace QuicKE_Admin
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginPage : Page
    {
        public string Message { get; set; }
        public bool IsValid { get; set; }
        public LoginPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            QuicKERuntime.LogonToken = await SettingItem.GetValueAsync("LogonToken");
            if(QuicKERuntime.HasLogonToken)
                Frame.Navigate(typeof(HomePage));          
        }



        public async void SignUp(object sender, RoutedEventArgs e)
        {
            ErrorBucket errors = new ErrorBucket();
            Validate(errors);
            // get a handler...
            var proxy = TinyIoCContainer.Current.Resolve<ISignInServiceProxy>();
            var result = await proxy.SignInAsync(UserName.Text, Password.Password);
            if (!(result.HasErrors))
            {
                string LastUserPhoneNumber = await SettingItem.GetValueAsync("LastUserPhoneNumber");
                QuicKERuntime.LogonToken = result.Token;
                if (string.IsNullOrEmpty(LastUserPhoneNumber))
                {
                    var proxy2 = TinyIoCContainer.Current.Resolve<IGetMyProfileServiceProxy>();
                    var user = await proxy2.GetProfileAsync();
                    if (!(user.HasErrors))
                    {
                        QuicKERuntime.UserDatabaseConnectionString = string.Format("QuicKEUser-{0}.db", user.Profile.phone);
                        var conn = QuicKERuntime.GetUserDatabase();
                        await conn.CreateTableAsync<UserItem>();
                        await UserItem.SetValueAsync("token", result.Token);
                        await UserItem.SetValueAsync("phonenumber", user.Profile.phone);
                        await UserItem.SetValueAsync("email", user.Profile.email);
                        await UserItem.SetValueAsync("fullname", user.Profile.name);
                        await UserItem.SetValueAsync("password", Password.Password);
                        await conn.CreateTableAsync<ExpertItem>();
                        ExpertItem expert = new ExpertItem() { 
                        NativeId = user.Profile.NativeId,
                        name = user.Profile.name,
                        

                        };
                       
                        var conn1 = QuicKERuntime.GetSystemDatabase();
                        await SettingItem.SetValueAsync("LastUserPhoneNumber", user.Profile.phone);
                        await SettingItem.SetValueAsync("LogonToken", result.Token);
                    }
                    else
                    {
                        errors.CopyFrom(user);
                    }

                }
                if (!errors.HasErrors)
                {
                    Frame.Navigate(typeof(HomePage));
                }
            }
            else
            {
                errors.CopyFrom(result);
            }


            // errors?
            if (errors.HasErrors)
                await PageExtender.ShowAlertAsync(errors);
        }

        private void Validate(ErrorBucket errors)
        {
            if (UserName.Text.Length < 1)
                errors.AddError("Username is required");
            if (Password.Password.Length < 6)
                errors.AddError("Password must be at least 6 characters");

        }

    }
}
