﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace QuicKE_Admin
{
    public class ServiceExecuteResult : ErrorBucket
    {
        public JObject Output { get; private set; }
          internal ServiceExecuteResult(JObject output)
        {
            this.Output = output;
        }

        internal ServiceExecuteResult(JObject output, string error)
            : this(output)
        {
            this.AddError(error);
        }
        internal ServiceExecuteResult(JObject output, List<string> errorlist)
            : this(output)
        {
            this.Copy(errorlist);
        }
    }
    public class ServiceExecuteResult2 : ErrorBucket
    {
        public string Jsonstring { get; private set; }
       
        internal ServiceExecuteResult2(string output)
        {
            this.Jsonstring = output;
        }

        internal ServiceExecuteResult2(string output, string error)
            : this(output)
        {
            this.AddError(error);
        }

    }
}
