﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuicKE_Admin
{
    public class GetTicketsResult : ErrorBucket
    {
        internal TicketData Ticket { get; set; }

        internal GetTicketsResult(TicketData item)
        {
            
            this.Ticket = new TicketData();
            this.Ticket = item;
        }

        internal GetTicketsResult(ErrorBucket bucket)
            : base(bucket)
        {
        }
    }
}
