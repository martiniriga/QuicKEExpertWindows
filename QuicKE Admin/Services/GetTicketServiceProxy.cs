﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace QuicKE_Admin
{
    public class GetTicketServiceProxy : ServiceProxy, IGetTicketServiceProxy
    {
        public GetTicketServiceProxy()
            : base("expert/tickets?token=" + QuicKERuntime.LogonToken)
        {
        }

        public async Task<GetTicketsResult> GetTicketsAsync()
        {

            // call...
            var jsonstring = await this.GetAsync2();

            //errors?
            if (jsonstring.HasErrors)
                return new GetTicketsResult(jsonstring);

            RootObject obj = JsonConvert.DeserializeObject<RootObject>(jsonstring.Jsonstring);
            TicketData data = new TicketData();
            data = obj.data.FirstOrDefault();
            if (data == null)
            {
                jsonstring.AddError("No Pending Tickets");
                return new GetTicketsResult(jsonstring);
            }
            else             

                return new GetTicketsResult(data);
        }
    }
    //[ImplementPropertyChanged]
    public class Service 
    {
        public int id { get; set; }
        public string name { get; set; }
        public decimal cost { get; set; }
    }

    public class TicketData 
    {
        public int id { get; set; }
        public string ticket_no { get; set; }
        public string expert { get; set; }
        public string phone { get; set; }
        public string id_number { get; set; }
        public string photo { get; set; }
        public string badge { get; set; }
        public string status { get; set; }
        public string location { get; set; }
        public string booking_date { get; set; }
        public decimal cost { get; set; }
        public ObservableCollection<Service> services { get; set; }
       

    }
    public class RootObject
    {
        public string status { get; set; }
        public List<TicketData> data { get; set; }
    }

   
}
