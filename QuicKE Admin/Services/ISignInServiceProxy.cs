﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuicKE_Admin
{
    public interface ISignInServiceProxy : IServiceProxy
    {
        Task<SignInResult> SignInAsync(string username, string password);
    }
}
