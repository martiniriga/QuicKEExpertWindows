﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace QuicKE_Admin
{
    public class CompleteTicketServiceProxy : ServiceProxy, ICompleteTicketServiceProxy
    {
         public CompleteTicketServiceProxy()
            : base("expert/tickets/"+ HomePageViewModel.TicketId +"/complete")
        {
        }

        public async Task<GetTicketsResult> GetTicketsAsync()
        {
            JsonObject input = new JsonObject();
            // call...
            var jsonstring = await this.PostAsync2(input);

            //errors?
            if (jsonstring.HasErrors)
                return new GetTicketsResult(jsonstring);

            RootObject obj = JsonConvert.DeserializeObject<RootObject>(jsonstring.Jsonstring);
            TicketData data = new TicketData();
            data = obj.data.FirstOrDefault();
                return new GetTicketsResult(data);
        }
    }
}
