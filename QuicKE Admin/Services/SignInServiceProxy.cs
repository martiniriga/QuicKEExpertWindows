﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace QuicKE_Admin
{
    public class SignInServiceProxy : ServiceProxy, ISignInServiceProxy
    {
        public SignInServiceProxy()
            : base("auth")
        {
        }

        public async Task<SignInResult> SignInAsync(string username, string password)
        {
            // package up the request...
            JsonObject input = new JsonObject();
            input.Add("username", username);
            input.Add("password", password);

            // call...
            var executeResult = await this.PostAsync(input);

            // get the user ID from the server result...
            if (!(executeResult.HasErrors))
            {
                string token = (string)executeResult.Output["token"];
                string status = (string)executeResult.Output["status"];
                
                return new SignInResult(token);
            }
            else
                return new SignInResult(executeResult);
        }
    
    }
}
