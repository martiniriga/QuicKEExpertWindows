﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyIoC;

namespace QuicKE_Admin
{
    public class ExpertItem : ModelItem
    {
        // key field...
        [AutoIncrement(), PrimaryKey(), JsonIgnore]
        public int Id { get; set; }

        // other fields...
        [Unique, JsonProperty("id")]
        public int NativeId { get { return this.GetValue<int>(); } set { this.SetValue(value); } }

        public string name { get { return this.GetValue<string>(); } set { this.SetValue(value); } }

        public string id_number { get { return this.GetValue<string>(); } set { this.SetValue(value); } }
        public string email { get { return this.GetValue<string>(); } set { this.SetValue(value); } }

        public string phone { get { return this.GetValue<string>(); } set { this.SetValue(value); } }
        public string badge { get { return this.GetValue<string>(); } set { this.SetValue(value); } }
        public string profession { get { return this.GetValue<string>(); } set { this.SetValue(value); } }
        public string distance { get { return this.GetValue<string>(); } set { this.SetValue(value); } }
        public string time { get { return this.GetValue<string>(); } set { this.SetValue(value); } }
        public int age { get { return this.GetValue<int>(); } set { this.SetValue(value); } }


       

        public ExpertItem()
        {
        }

        public static async Task UpdateCacheFromServerAsync()
        {
            // create a expert proxy to call up to the server...
            var proxy = TinyIoCContainer.Current.Resolve<IGetMyProfileServiceProxy>();
            var result = await proxy.GetProfileAsync();

            // did it actually work?
            result.AssertNoErrors();

            // update...
            var conn = QuicKERuntime.GetSystemDatabase();
               // load the existing one, deleting it if we find it...
                var existing = await conn.Table<ExpertItem>().Where(v => v.NativeId == result.Profile.NativeId).FirstOrDefaultAsync();
                if (existing != null)
                    await conn.DeleteAsync(existing);

                // create...
                await conn.InsertAsync(result.Profile);
         }

        // reads the local cache and populates a collection...
        internal static async Task<ExpertItem> GetAllFromCacheAsync()
        {
            var conn = QuicKERuntime.GetSystemDatabase();
            return await conn.Table<ExpertItem>().FirstOrDefaultAsync();
        }

        // indicates whether the cache is empty...
        internal static async Task<bool> IsCacheEmpty()
        {
            var conn = QuicKERuntime.GetSystemDatabase();
            return (await conn.Table<ExpertItem>().FirstOrDefaultAsync()) == null;
        }


    }
}
