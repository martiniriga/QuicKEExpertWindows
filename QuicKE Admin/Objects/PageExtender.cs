﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Popups;

namespace QuicKE_Admin
{
    internal static class PageExtender
    {
         internal static IAsyncOperation<IUICommand> ShowAlertAsync(string message)
        {
            // show...
            MessageDialog dialog = new MessageDialog(message != null ? message : string.Empty);
            return dialog.ShowAsync();
        }
        internal static IAsyncOperation<IUICommand> ShowAlertAsync( ErrorBucket errors)
        {
            return ShowAlertAsync(errors.GetErrorsAsString());
        }

    }
}
