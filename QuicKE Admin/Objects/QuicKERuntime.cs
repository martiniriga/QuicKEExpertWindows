﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using TinyIoC;
using Windows.Networking.PushNotifications;
using System.Diagnostics;

namespace QuicKE_Admin
{
    public static class QuicKERuntime
    {
        public static string Module { get; private set; }
        // holds a reference to the logon token...
        public static string LogonToken { get; set; }
        internal static string Location { get; set; }

        // holds references to the database connections...
        internal const string SystemDatabaseConnectionString = "QuicKEAdmin.db";
        internal static string UserDatabaseConnectionString = null;

        // defines the base URL of our services...
        internal const string ServiceUrlBase = "http://139.59.186.10/mfundi/public/api/";


        // starts the application/sets up state...
        public static async void Start(string module)
        {
            Module = module;

            // initialize TinyIoC...
            TinyIoCContainer.Current.AutoRegister();

            // initialize the system database... a rare move to do this synchronously as we're booting up...
            //create all tables standard for all users
            var conn = GetSystemDatabase();
            await conn.CreateTableAsync<SettingItem>();
            //await conn.CreateTableAsync<ExpertItem>();

            //grab last registered/logged in user  
            string LastEmail = await SettingItem.GetValueAsync("LastEmail");
            string LastUserPhoneNumber = await SettingItem.GetValueAsync("LastUserPhoneNumber");
            //user had registered/logged in before
            if (!string.IsNullOrEmpty(LastUserPhoneNumber))
            {   
                //set userdbstring to use last logged user
                UserDatabaseConnectionString = string.Format("QuicKEUser-{0}.db", LastUserPhoneNumber);
                ////get location
                //Location = await UserItem.GetValueAsync("Location");
            }

            LogonToken = await SettingItem.GetValueAsync("LogonToken");
            
        }

        public static bool HasLogonToken
        {
            get
            {
                return !(string.IsNullOrEmpty(LogonToken));
            }
        }

        internal static async Task SignInAsync(string username, string password)
        {
            // set the database to be a user specific one... phonenumber
            // - for production you may prefer to use a hash)...
            UserDatabaseConnectionString = string.Format("MFundi-user-{0}.db", username);

            //// store the logon token...
            //LogonToken = token;

            //// initialize the database - has to be done async...
            var conn = GetUserDatabase();
             await conn.CreateTableAsync<UserItem>();
             //await UserItem.SetValueAsync("token", token);
             //await UserItem.SetValueAsync("phonenumber", phonenumber);
             //await UserItem.SetValueAsync("email", email);
             //await UserItem.SetValueAsync("fullname", fullname);
             await UserItem.SetValueAsync("password", password);
             //await conn.CreateTableAsync<TicketItem>();

             conn = GetSystemDatabase();
             //await SettingItem.SetValueAsync("LastUserPhoneNumber", phonenumber);
             //await SettingItem.SetValueAsync("LogonToken", token);
            //will check this value before sign in to detrmine how to recreate local table if user is not created on device
            //but has account already
             //await SettingItem.SetValueAsync("LastEmail", email);

        }



        internal static SQLiteAsyncConnection GetSystemDatabase()
        {
            return new SQLiteAsyncConnection(SystemDatabaseConnectionString);
        }

        internal static SQLiteAsyncConnection GetUserDatabase()
        {
            return new SQLiteAsyncConnection(UserDatabaseConnectionString);
        }

        
    }
}
